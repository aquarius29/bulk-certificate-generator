<?php

$zip = new ZipArchive();
$filename = "generated_images.zip";

if ($zip->open($filename, ZipArchive::OVERWRITE)!==TRUE) {
    exit("cannot open <$filename>\n");
}

$post_data = json_decode(file_get_contents('php://input'));

foreach($post_data as $entry){
    $participant = $entry->img_name;
    $data =  $entry->img_data;
	list($type, $data) = explode(';', $data);
	list(, $data)      = explode(',', $data);

    $encodedData = str_replace(' ','+',$data);
    $decocedData = base64_decode($encodedData);
	$zip->addFromString($participant . ".jpg" , $decocedData);
}
$zip->close();
echo '{"filename":"'. $filename . '"}';
?>